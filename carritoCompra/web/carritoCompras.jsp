<%-- 
    Document   : carritoCompras
    Created on : 27-oct-2019, 1:58:50
    Author     : luisangelmendozalucio
--%>

<%@page import="Controlador.CookiesManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="./css/header.css">
        <link rel="stylesheet" href="./css/carrito.css">
    </head>
    <body>
        <header class="header">
            <nav class="nav">
                <ul>
                    <%
                        CookiesManagement cookiesManagement = new CookiesManagement(request, response);
                        String userName = cookiesManagement.getCookie("userName");
                        String lastName = cookiesManagement.getCookie("lastName");
                        String role = cookiesManagement.getCookie("roleUser");
                    %>
                    <li>
                        <a href="#">Lista de Productos</a>
                    </li>
                    <li>
                        <a href="#">Carrito <span>( 1 )</span></a>
                    </li>
                    <li>
                        <a href="#">Compras</a>
                    </li>
                    <li>
                        <a href="#">Lista de usuarios</a>
                    </li>
                    <li>
                        <a href="#">Ventas</a>
                    </li>
                    <li>
                        <a href="#">Cerrar sesión</a>
                    </li>
                    <li>
                        <a href="#"> <%= userName + ' ' + lastName%> </a>
                    </li>
                </ul>
            </nav>
        </header>

        <h1>Shopping Cart</h1>

        <div class="shopping-cart">

            <div class="column-labels">
                <label class="product-image">Image</label>
                <label class="product-details">Product</label>
                <label class="product-price">Price</label>
                <label class="product-quantity">Quantity</label>
                <label class="product-removal">Remove</label>
                <label class="product-line-price">Total</label>
            </div>

            <div class="product">
                <div class="product-image">
                    <img src="./images/img1.jpg">
                </div>
                <div class="product-details">
                    <div class="product-title">Player old taco</div>
                    <p class="product-description">Descripcon del producto eda.</p>
                </div>
                <div class="product-price">12.99</div>
                <div class="product-quantity">
                    <input type="number" value="2" min="1">
                </div>
                <div class="product-removal">
                    <button class="remove-product">
                        Remove
                    </button>
                </div>
                <div class="product-line-price">25.98</div>
            </div>

            <div class="product">
                <div class="product-image">
                    <img src="./images/img2.jpg">
                </div>
                <div class="product-details">
                    <div class="product-title">layera space</div>
                    <p class="product-description">Descripcon del producto eda.</p>
                </div>
                <div class="product-price">45.99</div>
                <div class="product-quantity">
                    <input type="number" value="1" min="1">
                </div>
                <div class="product-removal">
                    <button class="remove-product">
                        Remove
                    </button>
                </div>
                <div class="product-line-price">45.99</div>
            </div>

            <div class="totals">
                <div class="totals-item">
                    <label>Subtotal</label>
                    <div class="totals-value" id="cart-subtotal">71.97</div>
                </div>
                <div class="totals-item">
                    <label>Iva (16%)</label>
                    <div class="totals-value" id="cart-tax">3.60</div>
                </div>
                <div class="totals-item totals-item-total">
                    <label>Total</label>
                    <div class="totals-value" id="cart-total">90.57</div>
                </div>
            </div>

            <button class="checkout">Comprar</button>

        </div>
    </body>
</html>
