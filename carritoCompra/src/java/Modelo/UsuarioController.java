/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Utils.Conexion;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author luisangelmendozalucio
 */
public class UsuarioController {
    public Usuario Login(String email, String password) {
        System.out.println(email + "" + password);
        Usuario usuario = null;
        
        try {
            CallableStatement callStatement = Conexion.getConnection().prepareCall("{call login(" + "'" +email + "'" + "," + "'" +password + "'" + ")}");
            ResultSet result = callStatement.executeQuery();
            
            while (result.next()) {
                usuario = new Usuario(result.getInt(1), result.getString(3), result.getString(2), result.getString(4), result.getString(5), result.getString(6));
            }
        } catch(SQLException e) {System.out.println("Error al inciar sesion " + e);}
        
        return usuario;
    }
}
