package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Controlador.CookiesManagement;

public final class carritoCompras_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/header.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/carrito.css\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <header class=\"header\">\n");
      out.write("            <nav class=\"nav\">\n");
      out.write("                <ul>\n");
      out.write("                    ");

                        CookiesManagement cookiesManagement = new CookiesManagement(request, response);
                        String userName = cookiesManagement.getCookie("userName");
                        String lastName = cookiesManagement.getCookie("lastName");
                        String role = cookiesManagement.getCookie("roleUser");
                    
      out.write("\n");
      out.write("                    <li>\n");
      out.write("                        <a href=\"#\">Lista de Productos</a>\n");
      out.write("                    </li>\n");
      out.write("                    <li>\n");
      out.write("                        <a href=\"#\">Carrito <span>( 1 )</span></a>\n");
      out.write("                    </li>\n");
      out.write("                    <li>\n");
      out.write("                        <a href=\"#\">Compras</a>\n");
      out.write("                    </li>\n");
      out.write("                    <li>\n");
      out.write("                        <a href=\"#\">Lista de usuarios</a>\n");
      out.write("                    </li>\n");
      out.write("                    <li>\n");
      out.write("                        <a href=\"#\">Ventas</a>\n");
      out.write("                    </li>\n");
      out.write("                    <li>\n");
      out.write("                        <a href=\"#\">Cerrar sesión</a>\n");
      out.write("                    </li>\n");
      out.write("                    <li>\n");
      out.write("                        <a href=\"#\"> ");
      out.print( userName + ' ' + lastName);
      out.write(" </a>\n");
      out.write("                    </li>\n");
      out.write("                </ul>\n");
      out.write("            </nav>\n");
      out.write("        </header>\n");
      out.write("\n");
      out.write("        <h1>Shopping Cart</h1>\n");
      out.write("\n");
      out.write("        <div class=\"shopping-cart\">\n");
      out.write("\n");
      out.write("            <div class=\"column-labels\">\n");
      out.write("                <label class=\"product-image\">Image</label>\n");
      out.write("                <label class=\"product-details\">Product</label>\n");
      out.write("                <label class=\"product-price\">Price</label>\n");
      out.write("                <label class=\"product-quantity\">Quantity</label>\n");
      out.write("                <label class=\"product-removal\">Remove</label>\n");
      out.write("                <label class=\"product-line-price\">Total</label>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"product\">\n");
      out.write("                <div class=\"product-image\">\n");
      out.write("                    <img src=\"./images/img1.jpg\">\n");
      out.write("                </div>\n");
      out.write("                <div class=\"product-details\">\n");
      out.write("                    <div class=\"product-title\">Player old taco</div>\n");
      out.write("                    <p class=\"product-description\">Descripcon del producto eda.</p>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"product-price\">12.99</div>\n");
      out.write("                <div class=\"product-quantity\">\n");
      out.write("                    <input type=\"number\" value=\"2\" min=\"1\">\n");
      out.write("                </div>\n");
      out.write("                <div class=\"product-removal\">\n");
      out.write("                    <button class=\"remove-product\">\n");
      out.write("                        Remove\n");
      out.write("                    </button>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"product-line-price\">25.98</div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"product\">\n");
      out.write("                <div class=\"product-image\">\n");
      out.write("                    <img src=\"./images/img2.jpg\">\n");
      out.write("                </div>\n");
      out.write("                <div class=\"product-details\">\n");
      out.write("                    <div class=\"product-title\">layera space</div>\n");
      out.write("                    <p class=\"product-description\">Descripcon del producto eda.</p>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"product-price\">45.99</div>\n");
      out.write("                <div class=\"product-quantity\">\n");
      out.write("                    <input type=\"number\" value=\"1\" min=\"1\">\n");
      out.write("                </div>\n");
      out.write("                <div class=\"product-removal\">\n");
      out.write("                    <button class=\"remove-product\">\n");
      out.write("                        Remove\n");
      out.write("                    </button>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"product-line-price\">45.99</div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"totals\">\n");
      out.write("                <div class=\"totals-item\">\n");
      out.write("                    <label>Subtotal</label>\n");
      out.write("                    <div class=\"totals-value\" id=\"cart-subtotal\">71.97</div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"totals-item\">\n");
      out.write("                    <label>Iva (16%)</label>\n");
      out.write("                    <div class=\"totals-value\" id=\"cart-tax\">3.60</div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"totals-item totals-item-total\">\n");
      out.write("                    <label>Total</label>\n");
      out.write("                    <div class=\"totals-value\" id=\"cart-total\">90.57</div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <button class=\"checkout\">Comprar</button>\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
