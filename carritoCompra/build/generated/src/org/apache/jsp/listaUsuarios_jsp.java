package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class listaUsuarios_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <title>JSP Page</title>\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/header.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/lista-usuarios.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/header.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/lista-usuarios.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/bootstrap.css\">\r\n");
      out.write("        <script src=\"./js/bootstrap.min.js\"></script>\r\n");
      out.write("    </head>\r\n");
      out.write("    <header class=\"header\">\r\n");
      out.write("            <nav class=\"nav\">\r\n");
      out.write("                <ul>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Lista de Productos</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Carrito <span>( 1 )</span></a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Compras</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Lista de usuarios</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Ventas</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Cerrar sesión</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Luis Angel Mendoza Lucio</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                </ul>\r\n");
      out.write("            </nav>\r\n");
      out.write("        </header>\r\n");
      out.write("\r\n");
      out.write("        <h1>Shopping Cart</h1>\r\n");
      out.write("\r\n");
      out.write("        <div class=\"shopping-cart\">\r\n");
      out.write("\r\n");
      out.write("            <div class=\"column-labels\">\r\n");
      out.write("                <label class=\"product-details\">Nombre</label>\r\n");
      out.write("                <label class=\"product-details\">Apellido</label>\r\n");
      out.write("                <label class=\"product-price\">Correo electronico</label>\r\n");
      out.write("                <label class=\"product-quantity\">Rol</label>\r\n");
      out.write("                <label class=\"product-edit\">Editar</label>\r\n");
      out.write("                <label class=\"product-removal\">Remover</label>\r\n");
      out.write("            </div>\r\n");
      out.write("            ");
 for (int i = 0; i < 6; i++) { 
      out.write("\r\n");
      out.write("            <div class=\"product\">\r\n");
      out.write("                <div class=\"product-details\">\r\n");
      out.write("                    <div class=\"product-title\">Luis Angel</div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"product-details\">\r\n");
      out.write("                    <div class=\"product-title\">Mendoza Lucio</div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"product-details\">\r\n");
      out.write("                    <div class=\"product-title\">luisangelmendozalucio@gmail.com</div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"product-details\">\r\n");
      out.write("                    <div class=\"product-title\">Client</div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"product-edit\">\r\n");
      out.write("                    <button class=\"edit-product\">\r\n");
      out.write("                        Editar\r\n");
      out.write("                    </button>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"product-removal\">\r\n");
      out.write("                    <button class=\"remove-product\">\r\n");
      out.write("                        Remover\r\n");
      out.write("                    </button>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            ");
}
      out.write("\r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("        <!-- Modal -->\r\n");
      out.write("    <div id=\"example\"  class=\"modalExample\">\r\n");
      out.write("            <div class=\"modal-dialog\" role=\"document\">\r\n");
      out.write("                <div class=\"content-modal modal-content\">\r\n");
      out.write("                    <div class=\"modal-header align-content-center\">\r\n");
      out.write("                        <h5 class=\"modal-title\" id=\"exampleModalCenterTitle\">Agregar Usuario</h5>\r\n");
      out.write("                        <!-- <a href=\"#\"><span aria-hidden=\"true\" >&times;</span> -->\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"modal-body\">\r\n");
      out.write("                        <form action=\"\">\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label for=\"nombre\">Nombre</label>\r\n");
      out.write("                                <input type=\"text\" class=\"form-control\" id=\"nombre\" placeholder=\"Nombre\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label for=\"apellidos\">Apellidos</label>\r\n");
      out.write("                                <input type=\"text\" class=\"form-control\" id=\"apellidos\" placeholder=\"Apellidos\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                              \r\n");
      out.write("                            <div class=\"form-row\">\r\n");
      out.write("                                <div class=\"form-group col-md-6\">\r\n");
      out.write("                                    <label for=\"ciudad\">Ciudad</label>\r\n");
      out.write("                                    <input type=\"text\" class=\"form-control\" id=\"ciudad\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group col-md-6\">\r\n");
      out.write("                                    <label for=\"rol\">Rol</label>\r\n");
      out.write("                                    <select id=\"rol\" class=\"form-control\">\r\n");
      out.write("                                        <option selected>...</option>\r\n");
      out.write("                                        <option>Administrador</option>\r\n");
      out.write("                                        <option>CLiente</option>\r\n");
      out.write("                                    </select>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label for=\"email\">Correo</label>\r\n");
      out.write("                                <input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Correo Electronico\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label for=\"contrasena\">Contraseña</label>\r\n");
      out.write("                                <input type=\"password\" class=\"form-control\" id=\"contrasena\" placeholder=\"Contraseña\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </form>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"modal-footer\">\r\n");
      out.write("                        <a href=\"#\" class=\"btn btn-info\">Cerrar</a>\r\n");
      out.write("                        <button type=\"button\" class=\"btn btn-primary\">Agregar</button>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
