<%-- 
    Document   : agregarProducto
    Created on : 12-nov-2019, 20:29:50
    Author     : luisangelmendozalucio
--%>

<%@page import="Controlador.CookiesManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="./css/header.css">
        <link rel="stylesheet" href="./css/index.css">
        <link rel="stylesheet" href="./css/agregar-producto.css">
    </head>
    <body>
        <header class="header">
            <nav class="nav">
                <ul>
                    <%
                        CookiesManagement cookiesManagement = new CookiesManagement(request, response);
                        String userName = cookiesManagement.getCookie("userName");
                        String lastName = cookiesManagement.getCookie("lastName");
                        String role = cookiesManagement.getCookie("roleUser");
                    %>
                    <li>
                        <a href="index.jsp">Lista de Productos</a>
                    </li>
                    <li>
                        <a href="#">Carrito <span>( 1 )</span></a>
                    </li>
                    <li>
                        <a href="#">Compras</a>
                    </li>
                    <li>
                        <a href="#">Lista de usuarios</a>
                    </li>
                    <li>
                        <a href="#">Ventas</a>
                    </li>
                    <li>
                        <a href="#">Cerrar sesión</a>
                    </li>
                    <li>
                        <a href="#"> <%= userName + ' ' + lastName%> </a>
                    </li>
                </ul>
            </nav>
        </header>

        <div class="container-products">

            <div class="product-card col-4">
                <div class="product-card-header">
                    Imagen del producto
                </div>
                <div class="product-card-body">
                    <img id="output" alt="" name="img">
                </div>
                <input type="file" onchange="changeImage(event)"/>
            </div>
            <div class="product-card info col-4">
                <p class="title mb-20 mt-20 d-flex flex-align-center flex-justify-center">Detalles del producto</p>
                
                    <label class="mb-10">Nombre</label>
                    <input class="mb-20 input-control" name="name" type="text" value="">

                    <label class="mb-10">Precio unitario</label>
                    <input class="input-control mb-20" type="number" name="price" value="">

                    <label class="mb-10">Unidad</label>
                    <input class="mb-20 input-control" name="unity" type="number" min="0" value="">

                    <label class="mb-10">Existencia</label>
                    <input class="mb-20 input-control" name="existence" type="number" min="0" value="">

                    <label class="mb-10">Ubicación</label>
                    <input class="mb-20 input-control" name="ubication" type="text" value="Ubicación en almacen" value="">

                    <label class="mb-10">Descripción</label>
                    <textarea cols="5" rows="6" name="description" placeholder="Descrición del producto" value=""></textarea>

                    <div class="product-card-footer pos-absolute bottom-0">
                        <a href="ServletControlador?action=productAdd" class="btn-add-car">Agregar</a>
                        <a href="ServletControlador?action=productUpdate" class="btn-edit">Editar</a>
                        <button class="btn-delete">Eliminar</button>
                    </div>
                
            </div>
        </div>
        <script type="text/javascript">
            function changeImage(event) {
                var reader = new FileReader();
                reader.onload = function () {
                    var output = document.getElementById('output');
                    output.src = reader.result;
                };
                reader.readAsDataURL(event.target.files[0]);
            }
        </script>
    </body>
</html>
