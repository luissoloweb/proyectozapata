/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author luisangelmendozalucio
 */
public class Conexion {

    public static Connection getConnection() {
        Connection conexion = null;

        try {

            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/bdcarrito", "root", "");

//            Class.forName("com.mysql.jdbc.Driver");
//            conexion = DriverManager.getConnection("jdbc:mysql://localhost:8889/bdcarrito", "root", "root");
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conexion = DriverManager.getConnection("jdbc:sqlserver://DELL-PC\\JVENTURASQL2017:3306;databaseName=bdcarritos;user=Cubos;password=root;");


            System.out.println("Conexion a la base de datos satisfactoria");
        } catch (Exception e) {
            System.out.println("Error en la conexion a la base de datos" + e);
        }

        return conexion;
    }

    public static void main(String[] args) {
        // TODO code application logic here
        Conexion.getConnection();

    }
}
