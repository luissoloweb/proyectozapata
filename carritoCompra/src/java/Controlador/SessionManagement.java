/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author luisangelmendozalucio
 */
public class SessionManagement {
    public static boolean isLogged(HttpServletRequest request, HttpServletResponse response) {
        // si ya se tiene la sesión en las cookies:  email y password. se debe redirigir a la pantallade bienvenida.
        // si no se tienen las cookies se debe mostrar el login
        boolean hasSession = false;
        CookiesManagement cookiesManagement = new CookiesManagement(request, response);
        String sessionEmail = cookiesManagement.getCookie("sessionEmail");
        String sessionPassword = cookiesManagement.getCookie("sessionPassword");
        if (!sessionEmail.equals("") && !sessionPassword.equals("")) {
            hasSession = true;
        }
        return hasSession;
    }
}
