<%-- 
    Document   : verProducto
    Created on : 27-oct-2019, 1:57:56
    Author     : luisangelmendozalucio
--%>

<%@page import="Controlador.CookiesManagement"%>
<%@page import="Modelo.Producto"%>
<%@page import="Modelo.ProductoController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="./css/header.css">
        <link rel="stylesheet" href="./css/index.css">
        <link rel="stylesheet" href="./css/ver-producto.css">
    </head>
    <body>
        <header class="header">
            <nav class="nav">
                <ul>
                    <%
                        CookiesManagement cookiesManagement = new CookiesManagement(request, response);
                        String userName = cookiesManagement.getCookie("userName");
                        String lastName = cookiesManagement.getCookie("lastName");
                        String role = cookiesManagement.getCookie("roleUser");
                    %>
                    <li>
                        <a href="index.jsp">Lista de Productos</a>
                    </li>
                    <li>
                        <a href="#">Carrito <span>( 1 )</span></a>
                    </li>
                    <li>
                        <a href="#">Compras</a>
                    </li>
                    <li>
                        <a href="#">Lista de usuarios</a>
                    </li>
                    <li>
                        <a href="#">Ventas</a>
                    </li>
                    <li>
                        <a href="#">Cerrar sesión</a>
                    </li>
                    <li>
                        <a href="#"> <%= userName + ' ' + lastName%> </a>
                    </li>
                </ul>
            </nav>
        </header>

        <div class="container-products">
            
            <% 
                ProductoController pc = new ProductoController();
                int id = Integer.parseInt((String)request.getParameter("id"));
                Producto prod = pc.getProducto(id);
            %>
            
            <div class="product-card col-4">
                <div class="product-card-header">
                    Producto
                </div>
                <div class="product-card-body">
                    <img src="./images/img1.jpg" />
                </div>
            </div>
            <div class="product-card info col-4">
                <p class="title mb-20 mt-20 d-flex flex-align-center flex-justify-center">Detalles del producto</p>
                <label class="mb-10">Nombre</label>
                <input class="mb-20 input-control" name="name" type="text" value="<%= prod.getNombre() %>">
                <label class="mb-10">Cantidad</label>
                <input class="mb-20 input-control" name="quantity" type="number" min="0" value="<%= prod.getUnidad() %>">
                <label class="mb-10">Precio unitario</label>
                <input class="input-control" type="number" name="price" value="<%= prod.getPrecio() %>">

                <div class="product-card-footer pos-absolute bottom-0">
                    <a href="ServletControlador?action=productUpdate&id=<%= prod.getCodigoProducto() %>" class="btn-edit">Editar</a>
                    <a href="ServletControlador?action=productDelete&id=<%= prod.getCodigoProducto() %>" class="btn-delete">Eliminar</a>
                </div>
            </div>
        </div>
    </body>
</html>
