<%-- 
    Document   : formularioRegistro
    Created on : 14/11/2019, 07:03:46 PM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./css/resetRegistro.css">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,900" rel="stylesheet">
	<link rel="stylesheet" href="./css/mainRegistro.css">
	<title>Formulario</title>
</head>
<body>

	<div class="container">
		<div class="form__top">
			<h2>Formulario <span>Registro</span></h2>
		</div>		
		<form class="form__reg" action="ServletRegistro" method="POST">
	    <input class="input" name="nombre" type="text" placeholder="Nombre" required autofocus>
            <input class="input" name="apellido" type="text" placeholder="Apellido" required>
            <input class="input" name="ciudad" type="text" placeholder="Ciudad" required>
            <input class="input" name="correo" type="email" placeholder="Email" required>
            <input class="input" name="contrasena" type="password" placeholder="Contraseña" required>
            <div class="btn__form">
            	<input class="btn__submit" type="submit" value="REGISTRAR">
            	<input class="btn__reset" type="reset" value="LIMPIAR">	
            </div>
		</form>
	</div>
	
</body>
</html>