/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Producto;
import Modelo.ProductoController;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author luisangelmendozalucio
 */
public class ServletControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String action = request.getParameter("action");
            ProductoController pc = new ProductoController();
            Producto prod = new Producto();
            
            switch (action) {
                case "productAdd":
                    String nom = request.getParameter("name");
                    String img = request.getParameter("img");
                    int prec = Integer.parseInt(request.getParameter("price"));
                    String desc = request.getParameter("description");
                    int uni = Integer.parseInt(request.getParameter("unity"));
                    int exi = Integer.parseInt(request.getParameter("existence"));
                    prod.setNombre(nom);
                    prod.setImagen(img);
                    prod.setPrecio(prec);
                    prod.setDescripcion(desc);
                    prod.setUnidad(uni);
                    prod.setExistencia(exi);
                    pc.addProducto(prod);
                    RequestDispatcher agregar = request.getRequestDispatcher("index.jsp");
                    agregar.forward(request, response);
                    break;
                    
                case "productVer":
                    request.setAttribute("id", request.getParameter("id"));
                    RequestDispatcher ver = request.getRequestDispatcher("verProducto.jsp");
                    ver.forward(request, response);
                    break;
                case "productUpdate":
                    int id = Integer.parseInt(request.getParameter("id"));
                    nom = request.getParameter("name");
                    img = request.getParameter("img");
                    prec = Integer.parseInt(request.getParameter("price"));
                    prod.setCodigoProducto(id);
                    prod.setNombre(nom);
                    prod.setImagen(img);
                    prod.setPrecio(prec);
                    pc.updateProducto(prod);
                    RequestDispatcher act = request.getRequestDispatcher("index.jsp");
                    act.forward(request, response);
                    break;
                case "productDelete":
                    id = Integer.parseInt(request.getParameter("id"));
                    prod.setCodigoProducto(id);
                    pc.deleteProduct(id);
                    RequestDispatcher del = request.getRequestDispatcher("index.jsp");
                    del.forward(request, response);
                    break;
                case "productAddCar":
                    // code block
                    break;
                default:
                // code block
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
