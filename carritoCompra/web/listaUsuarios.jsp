<%-- 
    Document   : listaUsuarios
    Created on : 27-oct-2019, 13:04:12
    Author     : luisangelmendozalucio
--%>

<%@page import="Controlador.CookiesManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="./css/header.css">
        <link rel="stylesheet" href="./css/lista-usuarios.css">
        <link rel="stylesheet" href="./css/header.css">
        <link rel="stylesheet" href="./css/lista-usuarios.css">
        <link rel="stylesheet" href="./css/bootstrap.css">
    </head>
    <header class="header">
            <nav class="nav">
                <ul>
                    <%
                        CookiesManagement cookiesManagement = new CookiesManagement(request, response);
                        String userName = cookiesManagement.getCookie("userName");
                        String lastName = cookiesManagement.getCookie("lastName");
                        String role = cookiesManagement.getCookie("roleUser");
                    %>
                    <li>
                        <a href="#">Lista de Productos</a>
                    </li>
                    <li>
                        <a href="#">Carrito <span>( 1 )</span></a>
                    </li>
                    <li>
                        <a href="#">Compras</a>
                    </li>
                    <li>
                        <a href="#">Lista de usuarios</a>
                    </li>
                    <li>
                        <a href="#">Ventas</a>
                    </li>
                    <li>
                        <a href="#">Cerrar sesión</a>
                    </li>
                    <li>
                        <a href="#"> <%= userName + ' ' + lastName%> </a>
                    </li>
                </ul>
            </nav>
        </header>

    <h1>Shopping Cart</h1>

    <div class="product-edit user-add">
        <a class="btn btn-info add-user" id="show-modal" href="#example">Agregar Usuario</a>
    </div>

    <div class="shopping-cart">

        <div class="column-labels">
            <label class="product-details">Nombre</label>
            <label class="product-details">Apellido</label>
            <label class="product-price">Correo electronico</label>
            <label class="product-quantity">Rol</label>
            <label class="product-edit">Editar</label>
            <label class="product-removal">Remover</label>
        </div>
        <% for (int i = 0; i < 6; i++) { %>
        <div class="product">
            <div class="product-details">
                <div class="product-title">Luis Angel</div>
            </div>
            <div class="product-details">
                <div class="product-title">Mendoza Lucio</div>
            </div>
            <div class="product-details">
                <div class="product-title">luisangelmendozalucio@gmail.com</div>
            </div>
            <div class="product-details">
                <div class="product-title">Client</div>
            </div>
            <div class="product-edit">
                <button class="edit-product">
                    Editar
                </button>
            </div>
            <div class="product-removal">
                <button class="remove-product">
                    Remover
                </button>
            </div>
        </div>
        <%}%>
    </div>

    <!-- Modal -->
    <div id="example"  class="modalExample">
        <div class="modal-dialog" role="document">
            <div class="content-modal modal-content">
                <div class="modal-header align-content-center">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Agregar Usuario</h5>
                    <!-- <a href="#"><span aria-hidden="true" >&times;</span> -->
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" placeholder="Nombre">
                        </div>
                        <div class="form-group">
                            <label for="apellidos">Apellidos</label>
                            <input type="text" class="form-control" id="apellidos" placeholder="Apellidos">
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="ciudad">Ciudad</label>
                                <input type="text" class="form-control" id="ciudad">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="rol">Rol</label>
                                <select id="rol" class="form-control">
                                    <option selected>...</option>
                                    <option>Administrador</option>
                                    <option>CLiente</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">Correo</label>
                            <input type="email" class="form-control" id="email" placeholder="Correo Electronico">
                        </div>
                        <div class="form-group">
                            <label for="contrasena">Contraseña</label>
                            <input type="password" class="form-control" id="contrasena" placeholder="Contraseña">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-info">Cerrar</a>
                    <button type="button" class="btn btn-primary">Agregar</button>
                </div>
            </div>
        </div>
    </div>
</html>
