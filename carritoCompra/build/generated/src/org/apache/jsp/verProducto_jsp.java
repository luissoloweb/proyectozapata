package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Modelo.Producto;
import Modelo.ProductoController;

public final class verProducto_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <title>JSP Page</title>\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/header.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/index.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/ver-producto.css\">\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        <header class=\"header\">\r\n");
      out.write("            <nav class=\"nav\">\r\n");
      out.write("                <ul>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"index.jsp\">Lista de Productos</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Carrito <span>( 1 )</span></a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Compras</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Lista de usuarios</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Ventas</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Cerrar sesión</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li>\r\n");
      out.write("                        <a href=\"#\">Luis Angel Mendoza Lucio</a>\r\n");
      out.write("                    </li>\r\n");
      out.write("                </ul>\r\n");
      out.write("            </nav>\r\n");
      out.write("        </header>\r\n");
      out.write("\r\n");
      out.write("        <div class=\"container-products\">\r\n");
      out.write("            \r\n");
      out.write("            <div class=\"product-card col-4\">\r\n");
      out.write("                <div class=\"product-card-header\">\r\n");
      out.write("                    ombre producto\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"product-card-body\">\r\n");
      out.write("                    <img src=\"./images/img1.jpg\" />\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div class=\"product-card info col-4\">\r\n");
      out.write("                <p class=\"title mb-20 mt-20 d-flex flex-align-center flex-justify-center\">Detalles del producto</p>\r\n");
      out.write("                <label class=\"mb-10\">Nombre</label>\r\n");
      out.write("                <input class=\"mb-20 input-control\" name=\"name\" type=\"text\" value=\"Nombre producto\">\r\n");
      out.write("                <label class=\"mb-10\">Cantidad</label>\r\n");
      out.write("                <input class=\"mb-20 input-control\" name=\"quantity\" type=\"number\" min=\"0\">\r\n");
      out.write("                <label class=\"mb-10\">Precio unitario</label>\r\n");
      out.write("                <input class=\"input-control\" type=\"text\" name=\"price\" value=\"$250\">\r\n");
      out.write("\r\n");
      out.write("                <div class=\"product-card-footer pos-absolute bottom-0\">\r\n");
      out.write("                    <button  class=\"btn-add-car\">Agregar</button>\r\n");
      out.write("                    <a href=\"ServletControlador?action=productUpdate\" class=\"btn-edit\">Editar</a>\r\n");
      out.write("                    <button class=\"btn-delete\">Eliminar</button>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
