/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import  Utils.Conexion;
import java.sql.*;
import java.util.ArrayList;
/**
 *
 * @author luisangelmendozalucio
 */
public class ProductoController {
 
    public ArrayList<Producto> getProductos() {
        ArrayList<Producto> productList = new ArrayList<>();
        
        try {
            CallableStatement callStatement = Conexion.getConnection().prepareCall("{call obtener_todos_productos()}");
            ResultSet result = callStatement.executeQuery();
            
            while (result.next()) {
                Producto product = new Producto(result.getInt(1), result.getString(2), result.getInt(3), result.getString(4));
                productList.add(product);
            }
        } catch(SQLException e) {System.out.println("error al obtener lista de productos " + e);}
        
        return productList;
    }
    
    public Producto getProducto(int id) {
        Producto product = null;
        
        try {
            CallableStatement callStatement = Conexion.getConnection().prepareCall("{call obtener_producto_id(" + id + ")}");
            ResultSet result = callStatement.executeQuery();
            
            while (result.next()) {
                product = new Producto(result.getInt(1), result.getString(2), result.getInt(3), result.getString(4));
            }
        } catch(SQLException e) {System.out.println("Error al obtener el producto " + e);}
        
        return product;
    }
    
    public void updateProducto(Producto prod) {
        String sql = "update producto set id="+prod.getCodigoProducto()+", nombre="+prod.getNombre()+", precio="+prod.getPrecio()+"where id="+prod.getCodigoProducto();
        try {
            PreparedStatement ps = Conexion.getConnection().prepareStatement(sql);
            ps.executeUpdate();
        } catch(SQLException e) {System.out.println("Error al obtener el producto " + e);}
        
    }
    
    public void addProducto(Producto prod){
        try{
            String sql = "insert into producto (id,nombre, precio, imagen, descripcion, unidad, existencia)"
                    + " values("+10+",'"+prod.getNombre()+"',"+prod.getPrecio()+",'"+prod.getImagen()+"','"+prod.getDescripcion()+"',"+prod.getUnidad()+","+prod.getExistencia()+")";
            PreparedStatement ps = Conexion.getConnection().prepareStatement(sql);
            ps.executeUpdate();
        }catch(SQLException e){
            System.out.println("Error al insertar producto " + e);
        }
    }
    
    public void deleteProduct(int id){
        try {
            String sql = "delete from producto where id="+id;
            PreparedStatement ps = Conexion.getConnection().prepareStatement(sql);
            ps.executeUpdate();
        }catch(SQLException e){
            System.out.println("Error al eliminar producto " + e);
        }
        
    }
    
   
}
