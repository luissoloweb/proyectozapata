<%-- 
    Document   : detallesCompras
    Created on : 27-oct-2019, 1:59:24
    Author     : luisangelmendozalucio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="./css/header.css">
        <link rel="stylesheet" href="./css/carrito.css">
    </head>
    <body>
        <header class="header">
            <nav class="nav">
                <ul>
                    <li>
                        <a href="#">Lista de Productos</a>
                    </li>
                    <li>
                        <a href="#">Carrito <span>( 1 )</span></a>
                    </li>
                    <li>
                        <a href="#">Compras</a>
                    </li>
                    <li>
                        <a href="#">Lista de usuarios</a>
                    </li>
                    <li>
                        <a href="#">Ventas</a>
                    </li>
                    <li>
                        <a href="#">Cerrar sesión</a>
                    </li>
                    <li>
                        <a href="#">Luis Angel Mendoza Lucio</a>
                    </li>
                </ul>
            </nav>
        </header>

        <h1>Shopping Cart</h1>

        <div class="shopping-cart">

            <div class="column-labels">
                <label class="product-details">ID venta</label>
                <label class="product-price">Fecha de compra</label>
                <label class="product-quantity">Cantidad</label>
                <label class="product-removal">Ver detalles</label>
            </div>

            <div class="product">
                <div class="product-details">
                    <div class="product-title">1n2n31n2j</div>
                    <p class="product-description">Identificador unico de la venta</p>
                </div>
                <div class="product-price">17/05/19</div>
                <div class="product-quantity">
                    $456
                </div>
                <div class="product-removal">
                    <button class="remove-product">
                        Ver
                    </button>
                </div>
            </div>

            <div class="product">
                <div class="product-details">
                    <div class="product-title">1n2n31n2j</div>
                    <p class="product-description">Identificador unico de la venta</p>
                </div>
                <div class="product-price">17/05/19</div>
                <div class="product-quantity">
                    $456
                </div>
                <div class="product-removal">
                    <button class="remove-product">
                        Ver
                    </button>
                </div>
            </div>

        </div>
    </body>
</html>
