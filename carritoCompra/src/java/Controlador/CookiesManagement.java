/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author luisangelmendozalucio
 */
public class CookiesManagement {
    Cookie[] cookies;
    HttpServletRequest req;
    HttpServletResponse res;

    public CookiesManagement(HttpServletRequest req, HttpServletResponse res) {
        this.req = req;
        this.res = res;
        this.cookies = req.getCookies();
    }

    public void setCookie(String name, String value, int age) {
        // verificar que no exista la cookie
        boolean exist = false;
        for (Cookie _cookie : cookies) {
            if (_cookie.getName().equals(null)) {
                exist = true;
            }
        }

        if (exist) {
            // se va a modificar la cookie

        } else {
            // se va a crear la cookie
            Cookie cookie = new Cookie(name, value);
            cookie.setMaxAge(age);
            res.addCookie(cookie);
        }
    }

    public String getCookie(String name) {
        for (Cookie _cookie : cookies) {
            if (_cookie.getName().equals(name)) {
                return _cookie.getValue();
            }
        }
        return "";
    }

    public void deleteCookie(String name, String path) {
        Cookie cookies[] = req.getCookies();
        for (Cookie _cookie : cookies) {
            if (_cookie.getName().equals(name)) {
                _cookie.setMaxAge(0);
                res.addCookie(_cookie);
            }
        }
    }
}
