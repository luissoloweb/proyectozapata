package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class formularioRegistro_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("<head>\n");
      out.write("\t<meta charset=\"UTF-8\">\n");
      out.write("\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("\t<link rel=\"stylesheet\" href=\"./css/resetRegistro.css\">\n");
      out.write("\t<link href=\"https://fonts.googleapis.com/css?family=Lato:400,900\" rel=\"stylesheet\">\n");
      out.write("\t<link rel=\"stylesheet\" href=\"./css/mainRegistro.css\">\n");
      out.write("\t<title>Formulario</title>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("\t<div class=\"container\">\n");
      out.write("\t\t<div class=\"form__top\">\n");
      out.write("\t\t\t<h2>Formulario <span>Registro</span></h2>\n");
      out.write("\t\t</div>\t\t\n");
      out.write("\t\t<form class=\"form__reg\" action=\"ServletRegistro\" method=\"POST\">\n");
      out.write("\t    <input class=\"input\" name=\"nombre\" type=\"text\" placeholder=\"Nombre\" required autofocus>\n");
      out.write("            <input class=\"input\" name=\"apellido\" type=\"text\" placeholder=\"Apellido\" required>\n");
      out.write("            <input class=\"input\" name=\"rol\" type=\"text\" placeholder=\"Rol\" required>\n");
      out.write("            <input class=\"input\" name=\"ciudad\" type=\"text\" placeholder=\"Ciudad\" required>\n");
      out.write("            <input class=\"input\" name=\"correo\" type=\"email\" placeholder=\"Email\" required>\n");
      out.write("            <input class=\"input\" name=\"contrasena\" type=\"text\" placeholder=\"Contraseña\" required>\n");
      out.write("            <div class=\"btn__form\">\n");
      out.write("            \t<input class=\"btn__submit\" type=\"submit\" value=\"REGISTRAR\">\n");
      out.write("            \t<input class=\"btn__reset\" type=\"reset\" value=\"LIMPIAR\">\t\n");
      out.write("            </div>\n");
      out.write("\t\t</form>\n");
      out.write("\t</div>\n");
      out.write("\t\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
