package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"./css/login.css\">\r\n");
      out.write("    <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\r\n");
      out.write("         width=\"800px\" height=\"600px\" viewBox=\"0 0 800 600\" enable-background=\"new 0 0 800 600\" xml:space=\"preserve\">\r\n");
      out.write("    <linearGradient id=\"SVGID_1_\" gradientUnits=\"userSpaceOnUse\" x1=\"174.7899\" y1=\"186.34\" x2=\"330.1259\" y2=\"186.34\" gradientTransform=\"matrix(0.8538 0.5206 -0.5206 0.8538 147.9521 -79.1468)\">\r\n");
      out.write("    <stop  offset=\"0\" style=\"stop-color:#FFC035\"/>\r\n");
      out.write("    <stop  offset=\"0.221\" style=\"stop-color:#F9A639\"/>\r\n");
      out.write("    <stop  offset=\"1\" style=\"stop-color:#E64F48\"/>\r\n");
      out.write("    </linearGradient>\r\n");
      out.write("    <circle fill=\"url(#SVGID_1_)\" cx=\"266.498\" cy=\"211.378\" r=\"77.668\"/>\r\n");
      out.write("    <linearGradient id=\"SVGID_2_\" gradientUnits=\"userSpaceOnUse\" x1=\"290.551\" y1=\"282.9592\" x2=\"485.449\" y2=\"282.9592\">\r\n");
      out.write("    <stop  offset=\"0\" style=\"stop-color:#FFA33A\"/>\r\n");
      out.write("    <stop  offset=\"0.0992\" style=\"stop-color:#E4A544\"/>\r\n");
      out.write("    <stop  offset=\"0.9624\" style=\"stop-color:#00B59C\"/>\r\n");
      out.write("    </linearGradient>\r\n");
      out.write("    <circle fill=\"url(#SVGID_2_)\" cx=\"388\" cy=\"282.959\" r=\"97.449\"/>\r\n");
      out.write("    <linearGradient id=\"SVGID_3_\" gradientUnits=\"userSpaceOnUse\" x1=\"180.3469\" y1=\"362.2723\" x2=\"249.7487\" y2=\"362.2723\">\r\n");
      out.write("    <stop  offset=\"0\" style=\"stop-color:#12B3D6\"/>\r\n");
      out.write("    <stop  offset=\"1\" style=\"stop-color:#7853A8\"/>\r\n");
      out.write("    </linearGradient>\r\n");
      out.write("    <circle fill=\"url(#SVGID_3_)\" cx=\"215.048\" cy=\"362.272\" r=\"34.701\"/>\r\n");
      out.write("    <linearGradient id=\"SVGID_4_\" gradientUnits=\"userSpaceOnUse\" x1=\"367.3469\" y1=\"375.3673\" x2=\"596.9388\" y2=\"375.3673\">\r\n");
      out.write("    <stop  offset=\"0\" style=\"stop-color:#12B3D6\"/>\r\n");
      out.write("    <stop  offset=\"1\" style=\"stop-color:#7853A8\"/>\r\n");
      out.write("    </linearGradient>\r\n");
      out.write("    <circle fill=\"url(#SVGID_4_)\" cx=\"482.143\" cy=\"375.367\" r=\"114.796\"/>\r\n");
      out.write("    <linearGradient id=\"SVGID_5_\" gradientUnits=\"userSpaceOnUse\" x1=\"365.4405\" y1=\"172.8044\" x2=\"492.4478\" y2=\"172.8044\" gradientTransform=\"matrix(0.8954 0.4453 -0.4453 0.8954 127.9825 -160.7537)\">\r\n");
      out.write("    <stop  offset=\"0\" style=\"stop-color:#FFA33A\"/>\r\n");
      out.write("    <stop  offset=\"1\" style=\"stop-color:#DF3D8E\"/>\r\n");
      out.write("    </linearGradient>\r\n");
      out.write("    <circle fill=\"url(#SVGID_5_)\" cx=\"435.095\" cy=\"184.986\" r=\"63.504\"/>\r\n");
      out.write("    </svg>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <div class=\"container\">\r\n");
      out.write("        <h2>login</h2>\r\n");
      out.write("        <form action=\"ServletLogueo\" method=\"post\">\r\n");
      out.write("            <input type=\"text\" class=\"email\" placeholder=\"Correo\" name=\"email\">\r\n");
      out.write("            <br/>\r\n");
      out.write("            <input type=\"password\" class=\"pwd\" placeholder=\"Contraseña\" name=\"password\">\r\n");
      out.write("\r\n");
      out.write("            <button class=\"register\">\r\n");
<<<<<<< HEAD
      out.write("                <span>Registrate</span>\r\n");
=======
      out.write("                <a href=\"formularioRegistro.jsp\">Registrate</a> \r\n");
>>>>>>> fb275f2f08aadc348083172d8932520955d5061a
      out.write("            </button>\r\n");
      out.write("            <input type=\"submit\" class=\"signin\" value=\"Iniciar sesión\">\r\n");
      out.write("        </form>\r\n");
      out.write("        <a href=\"#\" class=\"link\">\r\n");
      out.write("            Olvidaste tu Contraseña?\r\n");
      out.write("        </a>\r\n");
      out.write("        <br/>\r\n");
      out.write("        <h3>Registro Completado!    </h3>\r\n");
      out.write("        <h3>Inicio de Sesion Satisfactorio!</h3>\r\n");
      out.write("        <div class=\"listaUsuarios\"></div>\r\n");
      out.write("        <div class=\"carritoCompras\"></div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    </div>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
